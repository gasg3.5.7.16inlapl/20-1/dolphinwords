<?php
    $conn = mysqli_connect("localhost", "root", "admin123", "Dictionary");
    $word=$_GET['word'];
    switch(strtoupper($_SERVER['REQUEST_METHOD'])){
        case 'GET':
        $query = "SELECT e.Word, e.fecha_de_registro, d.Description, d.Source, f.String, s.Word Synonym, a.Translation 
        from Word e 
        inner join Word_has_definition_type l on l.Word_idWord=e.idWord 
        inner join Definition d on d.idDefinition=l.Definition_idDefinition
        inner join Type f on f.idType=l.Type_idType
        inner join Word_has_Synonym t on t.Word_idWord=e.idWord
        inner join Word s on s.idWord=t.Word_idWordSynonym
        inner join Word_has_translation r on r.Word_idWord=e.idWord
        inner join Translation a on a.idTranslation=r.Translation_idTranslation
        WHERE e.Word=".$word;
        $resultados= mysqli_query($conn, $query) or die ('Unable to execute query.'. mysqli_error($conn));
        if ($resultados->num_rows > 0) {
            while($row = $resultados->fetch_assoc()) {
                echo "Word: " . $row["Word"]. " - Date: " . $row["fecha_de_registro"]."<br>"."Description: ". $row["Description"]."<br>"."Source: ".$row["Source"]."<br>"."Type: ".$row["String"]."<br>"."Synonym: ".$row["Synonym"]."<br>"."Translation: ".$row["Translation"]."<br>";
            }
        }
        else {
            echo "0 results";
        }
        break;
    }
    $conn->close();

?>