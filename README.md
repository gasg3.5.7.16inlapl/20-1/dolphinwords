- Project summary:
Our project is an API rest which provides words and their respective meaning. This project was made in order to help people who need to expand their vocabulary regarding computer science. Our goal is that this API be reused and complemented with other projects that have the same vision
- Database Design:
Our database has 7 tables which are: 
- •	Word: 
-     •	IdWord (int)
-     •	Word (varchar)
-     •	fecha_de_registro (date)
- •	Type: (e.g verb, noun, verb, adjective, adverb, prepositions, conjunction)
-     •	idType (int)
-     •	String (varchar)
- •	Translation:
-     •	idTranslation (int)
-     •	Translation (varchar)
- •	Definition:
-     •	idDefinition (int)
-     •	Description (varchar)
-     •	Source (varchar)
- •	Word_has_definition_type: (Intermediate table where you can find the word type)
-     •	Definition_idDefinition (int)
-     •	Type_idType (int)
-     •	Word_idWord (int)
- •	Word_has_Synonym: (Intermediate table where you can find the synonym of a word)
-     •	Word_idWord (int)
-     •	Word_idWordSynonym (int)
- •	Word_has_translation: (Intermediate table where you can find the translation of a word)
-     •	Tranlation_idTranlation (int)
-     •	Word_idWord (int)

- Query links:
- If you want to see the information about a word:
    - http://peluquerialenin.com/search.php?word=%22custom%22
    - http://peluquerialenin.com/search.php?word=%22after%22
    - http://peluquerialenin.com/search.php?word=%22file%22
- If you want to see a specific table:
    - http://peluquerialenin.com/requests.php?resource_type=words
    - http://peluquerialenin.com/requests.php?resource_type=types
    - http://peluquerialenin.com/requests.php?resource_type=definitions
    - http://peluquerialenin.com/requests.php?resource_type=translations
    - http://peluquerialenin.com/requests.php?resource_type=synonyms
- Or specific row:
    - http://peluquerialenin.com/requests.php?resource_type=words&resource_id=1
- Postman: (URL: http://peluquerialenin.com/search.php, enter this into the body)

{"idWord": 85 ,"Word":"'Software'","registration_date":"'2020-06-10'","idDefinition":64,"Description":"'Computer software, or simply software, is a collection of data or computer instructions that tell the computer how to work.'","Source":"'https://en.wikipedia.org/wiki/Software'","idType":1,"idTranslation":64,"Translation":"'Programa'"}

